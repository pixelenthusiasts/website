const express = require('express');
const markoExpress = require('marko/express');
require('marko/node-require'); // Allow Node.js to require and load `.marko` files

require('lasso').configure({
  "plugins": [
      "lasso-marko"
  ]
});

//TODO: A helper function that can initialize all components in these directories would be great
let counter = require('./components/counter')
let testpage = require('./pages/testpage')

let app = express();
app.use(markoExpress());

app.use('/static', express.static(__dirname + '/static'));

app.get('/', function(req, res) {
  res.marko(testpage);
});

app.listen(8080);
console.log("App started on 8080");